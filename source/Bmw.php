<?php

namespace Car;

class Bmw extends Car {

    public function __construct($brand = "", $license = "", $volume, $volume = "") {
        parent::__construct($brand, $license);
        $this->calculate_tank_volume($volume);
    }

    public function calculate_tank_volume($volume) {
        $this->tank = $volume['height'] * $volume['width'] * $volume['length'];
    }

}

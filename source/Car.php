<?php

namespace Car;

abstract class Car {
    public $color = "";
    public $license = "";
    public $millage = "";
    protected $tank = 0.0;
    public $approximate_distence = 0;
    public $brand;
    public $model;
    static public $counter = 0;

    const PI = 3.141;

    public function __construct($brand = "", $license = "") {
        $this->brand = $brand;
        $this->license = $license;
        self::$counter++;
    }

    public function __toString() {

        return "The Car's Color is:" . $this->color .
                "<br/>The Car's License is:" . $this->license .
                "<br/>The Car's millage is:" . $this->millage . "mpg" .
                "<br/>The Car's Current fuel is:" . $this->tank . "Gallon" .
                "<br/>The Car's Approximate distence is:" . $this->approximate_distence() . "Mile" .
                "<br/>The Car's Brand is:" . $this->brand .
                "<br/>The Car's Model is:" . $this->model;
    }

    public function filling($fuel) {
//      $this->tank = $this->tank + $fuel;
        $this->tank +=$fuel;
//      $this->approximate_distence();
        return $this;
    }

    public function approximate_distence() {
        $this->approximate_distence = $this->tank * $this->millage;
        return $this->approximate_distence;
    }

    public function ride($distence = 0) {
        $this->tank = $this->tank - $distence / $this->millage;
        $this->tank -= $distence / $this->millage;
        return $this->tank;
        return $this;
    }

    public function set_model($model) {
        $this->model = $model;
    }

    abstract public function calculate_tank_volume($volume);

    public function get_tank_volume() {
        return $this->tank;
    }

}

<?php

namespace car;

class Toyota extends Car {

    public function __construct($brand = "", $license = "", $volume, $volume = "") {
        parent::__construct($brand, $license);
        $this->calculate_tank_volume($volume);
    }

    public function calculate_tank_volume($volume) {

        $this->tank = parent:: PI * pow($volume['radius'], 2) * $volume['height'];
    }

}

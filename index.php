<?php
include_once('vendor/autoload.php');

use Car\Bmw;

$bmw_1 = new Bmw("BMW", "Dhaka-KA-2309", 33, ['width' => 10, 'height' => 5, 'length' => 10]);
$bmw_2 = new Bmw("BMW", "Dhaka-JA-3000", 13, ['width' => 12, 'height' => 10, 'length' => 5]);

// $bmw_1->filling(50)
//        ->ride(10)
//        ->approximate_distence();
//$bmw_1->set_model('BMW-1100');
//$car1 = new Car('sdfsdf','hfgh');
use Car\Toyota;

$toyota_1 = new Toyota("TOYOTA", "Dhaka-TA-1120", 25, ['radius' => 5, 'height' => 10]);
$toyota_2 = new Toyota("TOYOTA", "Dhaka-Ga-1520", 30, ['radius' => 2, 'height' => 15]);
$toyota_3 = new Toyota("TOYOTA", "Dhaka-Ga-1520", 30, ['radius' => 2, 'height' => 15]);

//$toyota_1->filling(40)
//              ->ride(5)
//              ->approximate_distence();
//$toyota_1->set_model('TOYOTA-Primio');



use Car\Company;

$anz_company = new Company('ANZ Co.');
//var_dump($bmw_1 instanceof Bmw);
$anz_company->Include_cars($bmw_1);
$anz_company->Include_cars($bmw_2);

$anz_company->Include_cars($toyota_1);
$anz_company->Include_cars($toyota_2);
$anz_company->Include_cars($toyota_3);

//echo $anz_company->cars[0]->license;
//echo count($anz_company->cars);
////echo Car:: $counter;
//echo "<pre>";
//print_r($anz_company);
//exit();
?>

<html>
    <head>
        <title>Rented Car</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            table{ background-color:#ddd; }
            td{ padding:5px 10px;}
            th{padding:5px 10px; color:dodgerblue;}
        </style>
    </head>
    <body>

    <center><h1><?php echo $anz_company->name; ?></h1></center>
    <table border = 1  align = center >
        <tr>
            <th>Make</th>                
            <th>No. Of Car</th>
            <th>Tank Vol.</th>
            <!--<th>Gas Req</th>-->
            <th>Cost</th>

        </tr>
        <?php
        $total_cars = 0;
        $total_tank_volume = 0;
        $price = 100.00;
        $total_price = 0;
        foreach ($anz_company->cars as $brand => $car) {
            $total_cars += count($car);
            $total_tank_volume += $anz_company->calculate_total_tank_volume($brand);
            $total_price += $anz_company->calculate_total_tank_volume($brand) * $price;
            ?>
            <tr>

                <td><?php echo $brand ?></td>
                <td><?php echo count($car) ?></td>
                <td><?php echo $anz_company->calculate_total_tank_volume($brand); ?></td>
                <td><?php echo $anz_company->calculate_total_tank_volume($brand) * $price . " " . "BDT"; ?></td>

            </tr>

        <?php } ?>
        <tr>
            <td style="color: brown; font-weight: 600" >Grand Total</td>
            <td style="color: brown; font-weight: 600" ><?php echo $total_cars; ?></td>
            <td style="color: brown; font-weight: 600" ><?php echo $total_tank_volume; ?></td>
            <td style="color: brown; font-weight: 600" ><?php echo $total_price . " " . "BDT"; ?></td>

        </tr>


    </table>
</body>
</html>
